package avaj.tests;

import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

import org.mockito.Mock;
import org.mockito.Mockito;
import simulator.flyable.Aircraft;
import simulator.flyable.AircraftFactory;
import simulator.flyable.Coordinates;
import simulator.flyable.Flyable;
import simulator.tower.WeatherTower;

import java.lang.reflect.InvocationTargetException;

public class AircraftTests {

    @Test
    void move_checkLogic() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        WeatherTower weatherTower = Mockito.mock(WeatherTower.class);
        Mockito.when(weatherTower.getWeather(any(Coordinates.class))).thenReturn("SUN");

        Aircraft flyable = (Aircraft) AircraftFactory.newAircraft("Baloon", "ШАР", 10, 10, 10);

        flyable.move(weatherTower);

        Coordinates coordinates = (Coordinates) flyable.getClass().getField("coordinates").get(flyable);

        assertAll(
                () -> assertEquals(12, coordinates.getLongitude()),
                () -> assertEquals(10, coordinates.getLatitude()),
                () -> assertEquals(14, coordinates.getHeight())
        );

    }

    @Test
    void move_debuggerTest() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Flyable flyable = AircraftFactory.newAircraft("Baloon", "ШАР", 10, 10, 10);
        flyable.registerTower(new WeatherTower());

        flyable.updateConditions();

        assertNotNull(flyable, "Не удалось создать flyable"); //set breakpoint here
    }

}
