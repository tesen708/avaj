package avaj.tests;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.function.Executable;
import simulator.flyable.*;

import java.lang.reflect.InvocationTargetException;

public class AircfaftFactoryTests {

    @Test
    void newAircraft_canCreateAircraft() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Flyable flyable = AircraftFactory.newAircraft("Baloon", "ШАР", 10, 10, 10);

        assertNotNull(flyable, "Не удалось создать flyable");
    }

    @Test
    void newAircraft_checkChooseClass() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Flyable flyable = AircraftFactory.newAircraft("JetPlane", "самолет", 10, 10, 10);

        assertEquals(JetPlane.class, flyable.getClass(), "Несоответствие класса");
    }

    @Test
    void newAircraft_wrongType() {
        Executable toTest = () -> AircraftFactory.newAircraft("HeavyRock", "Тяжелый камешек", 10, 10, 10);

        assertThrows(ClassNotFoundException.class, toTest, "Ожидалось искючение");
    }

}
