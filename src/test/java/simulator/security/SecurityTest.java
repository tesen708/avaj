package simulator.security;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

class SecurityTest {

    static final String HASH_SUM = "e6e219eddaae29b43f1e5b6960ce80e9";
    @TempDir
    Path tempDir;
    Path scenarioPath;
    String scenarioContents;

    @BeforeEach
    void setUp() throws IOException {
        scenarioPath = tempDir.resolve("scenario.txt");
        Files.createFile(scenarioPath);
        scenarioContents = "25\n" +
                "Baloon B1 2 3 20\n" +
                "Baloon B2 1 8 66\n" +
                "JetPlane J1 23 44 32\n" +
                "Helicopter H1 654 33 20\n" +
                "Helicopter H2 22 33 44\n" +
                "Helicopter H3 98 68 99\n" +
                "Baloon B3 102 22 34\n" +
                "JetPlane J2 11 99 768\n" +
                "Helicopter H4 223 23 54";
        Files.write(scenarioPath, scenarioContents.getBytes());
    }

    @AfterEach
    void tearDown() {
        scenarioPath = null;
        scenarioContents = null;
    }

    @Test
    void hashFile() throws IOException, NoSuchAlgorithmException {

        String hashSum = Security.hashFile(scenarioPath.toString());

        assertEquals(HASH_SUM, hashSum);
    }

    @Test
    void checkHashSum_positive() throws IOException, NoSuchAlgorithmException {
        String hashSum = Security.hashFile(scenarioPath.toString());

        Executable executable = () -> Security.checkHashSum(scenarioPath.toString(), hashSum);

        assertDoesNotThrow(executable);
    }

    @Test
    void checkHashSum_negative() {
        String hashSum = "hashSum";

        Executable executable = () -> Security.checkHashSum(scenarioPath.toString(), hashSum);

        assertThrows(IllegalArgumentException.class, executable);
    }

}