package simulator.scenario;

import com.google.common.primitives.UnsignedInteger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import simulator.flyable.exceptions.InvalidContentException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ScenarioReaderTest {

    static final List<Integer> BASE_NUMBERS = Arrays.asList(
            Integer.MIN_VALUE, Integer.MIN_VALUE + 1,
            -1, 0, 1,
            Integer.MAX_VALUE - 1, Integer.MAX_VALUE
    );

    static final int ASCII_LAST_CHARACTER = 127;
    static final String NON_DIGIT_ASCII = IntStream.rangeClosed(0, ASCII_LAST_CHARACTER)
                    .filter(character ->
                            character != '-' && character != '='
                                    && !Character.isWhitespace(character)
                                    && !Character.isDigit(character))
                    .collect(StringBuilder::new,
                            StringBuilder::appendCodePoint,
                            StringBuilder::append)
                    .toString(); ;

    static final String HEX_PREFIX = "0x";
    static final String HEX_X_PREFIX = "0X";
    static final String HEX_HASH_PREFIX = "0X";
    static final String OCTAL_PREFIX = "0";
    static final String PLUS_PREFIX = "+";

    static final int UNSIGNED_INT_MAX_VALUE = 0xffffffff;

    static final int HEX = 16;
    static final int DECIMAL = 10;
    static final int OCTAL = 8;

    private static List<String> addRadixPrefix(List<?> list, String prefix) {
        return list.stream()
                .map(string -> (
                        string.toString().charAt(0) == '-'
                                ? "-" + prefix + string.toString().substring(1)
                                : prefix + string.toString()))
                .collect(Collectors.toList());
    }

    private static List<String> convertByRadix(List<? extends Integer> list, int radix) {
        return list.stream()
                .map(number -> Integer.toString(number, radix))
                .collect(Collectors.toList());
    }

    @ParameterizedTest
    @NullSource
    void createScenario_throwsNullPointerExceptionForNullInputString(String input) {
        Executable executable = () -> ScenarioReader.createScenario(input);

        assertThrows(NullPointerException.class, executable);
    }

    @ParameterizedTest
    @EmptySource
    void createScenario_throwsFileNotFoundExceptionWhenFileDoesNotExists(String input) {
        Executable executable = () -> ScenarioReader.createScenario(input);

        assertThrows(FileNotFoundException.class, executable);
    }

    @Test
    void createScenario_throwsFileNotFoundExceptionWhenCanNotReadFile(@TempDir Path tempDir) throws IOException {
        Path canNotRead = tempDir.resolve("canNotRead.txt");
        Files.createFile(
                canNotRead,
                PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("---------"))
        );

        Executable executable = () -> ScenarioReader.createScenario(canNotRead.toAbsolutePath().toString());

        assertThrows(FileNotFoundException.class, executable);
    }

    private static Stream<String> readSimulationRepetition_invalidNumberFormat() {
        List<String> wrongNumberStrings = BASE_NUMBERS.stream()
                .map(String::valueOf)
                .collect(Collectors.toList());
        for (int i = 0; i < wrongNumberStrings.size(); i++) {
            StringBuilder stringBuilder = new StringBuilder(wrongNumberStrings.get(i));
            char ch = NON_DIGIT_ASCII.charAt(new Random().nextInt(NON_DIGIT_ASCII.length()));
            int index = 0;
            if (stringBuilder.length() > 1) {
                index = new Random().nextInt(stringBuilder.length() - 1);
            }
            stringBuilder.setCharAt(index, ch);
            wrongNumberStrings.set(i, stringBuilder.toString());
        }
        wrongNumberStrings.add(Long.valueOf((long) Integer.MIN_VALUE - 1).toString());
        wrongNumberStrings.add(Long.valueOf((long) Integer.MAX_VALUE + 1).toString());
        wrongNumberStrings.add(Integer.toUnsignedString(UNSIGNED_INT_MAX_VALUE - 1));
        wrongNumberStrings.add(Integer.toUnsignedString(UNSIGNED_INT_MAX_VALUE));
        return wrongNumberStrings.stream();
    }

    @ParameterizedTest
    @MethodSource
    void readSimulationRepetition_invalidNumberFormat(String input) throws InvalidContentException {
        ScenarioReader scenarioReader = Mockito.mock(ScenarioReader.class);
        Mockito.when(scenarioReader.readSimulationRepetition(Mockito.anyString())).thenCallRealMethod();

        Executable executable = () -> scenarioReader.readSimulationRepetition(input);

        assertThrows(NumberFormatException.class, executable);
    }

    private static Stream<String> readSimulationRepetition_invalidContent() {
        List<Integer> filteredNumbers = BASE_NUMBERS.stream()
                .filter(integerNumber -> integerNumber < 0)
                .collect(Collectors.toList());
        List<String> decimalDigits = addRadixPrefix(convertByRadix(filteredNumbers, DECIMAL), "");
        List<String> hexDigits = addRadixPrefix(convertByRadix(filteredNumbers, HEX), HEX_PREFIX);
        List<String> hexXDigits = addRadixPrefix(convertByRadix(filteredNumbers, HEX), HEX_X_PREFIX);
        List<String> hexHashDigits = addRadixPrefix(convertByRadix(filteredNumbers, HEX), HEX_HASH_PREFIX);
        List<String> octalDigits = addRadixPrefix(convertByRadix(filteredNumbers, OCTAL), OCTAL_PREFIX);
        return Stream.of(
                decimalDigits, hexDigits, hexDigits, hexXDigits, hexHashDigits, octalDigits
        )
                .flatMap(List::stream);
    }

    @ParameterizedTest
    @MethodSource
    void readSimulationRepetition_invalidContent(String input) throws InvalidContentException {
        ScenarioReader scenarioReader = Mockito.mock(ScenarioReader.class);
        Mockito.when(scenarioReader.readSimulationRepetition(Mockito.anyString())).thenCallRealMethod();

        Executable executable = () -> scenarioReader.readSimulationRepetition(input);

        assertThrows(InvalidContentException.class, executable, "Here what input was: " + input);
    }

    private static Stream<String> readSimulationRepetition_positiveTest() {
        List<Integer> filteredNumbers = BASE_NUMBERS.stream()
                .filter(integerNumber -> integerNumber >= 0)
                .collect(Collectors.toList());
        List<String> decimalDigits = addRadixPrefix(convertByRadix(filteredNumbers, DECIMAL), "");
        List<String> hexDigits = addRadixPrefix(convertByRadix(filteredNumbers, HEX), HEX_PREFIX);
        List<String> hexXDigits = addRadixPrefix(convertByRadix(filteredNumbers, HEX), HEX_X_PREFIX);
        List<String> hexHashDigits = addRadixPrefix(convertByRadix(filteredNumbers, HEX), HEX_HASH_PREFIX);
        List<String> octalDigits = addRadixPrefix(convertByRadix(filteredNumbers, OCTAL), OCTAL_PREFIX);
        List<String> withLeadingPlusDigits = addRadixPrefix(convertByRadix(filteredNumbers, DECIMAL), PLUS_PREFIX);
        return Stream.of(
                decimalDigits, hexDigits, hexDigits, hexXDigits, hexHashDigits, octalDigits, withLeadingPlusDigits
        )
                .flatMap(List::stream);
    }

    @ParameterizedTest
    @MethodSource
    void readSimulationRepetition_positiveTest(String input) throws InvalidContentException {
        ScenarioReader scenarioReader = Mockito.mock(ScenarioReader.class);
        Mockito.when(scenarioReader.readSimulationRepetition(Mockito.anyString())).thenCallRealMethod();

        Executable executable = () -> scenarioReader.readSimulationRepetition(input);

        assertDoesNotThrow(executable);
    }

}