package simulator.security;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Security {

    static final String INVALID_HASH_SUM
            = "Друг мой, мне неловко говорить тебе такое, но хэшсумма файла не совпала с тем, что ты дал мне. "
            + "Случай неприятный, но я не буду никуда докладывать. "
            + "А ты, пожалуйтса, постарайся, очень настоятельно тебя прошу, всё исправить и как можно скорее.";

    public static String hashFile(String pathString) throws NoSuchAlgorithmException, IOException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(Files.readAllBytes(Paths.get(pathString)));
        byte[] digest = md5.digest();
        return encodeHexString(digest);
    }

    public static void checkHashSum(String pathString, String outerHashSum) throws IOException,
            NoSuchAlgorithmException, IllegalArgumentException {
        String myHashSum = hashFile(pathString);
        if (!myHashSum.equals(outerHashSum)) {
            throw new IllegalArgumentException(INVALID_HASH_SUM);
        }
    }

    static String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    static String encodeHexString(byte[] byteArray) {
        StringBuilder hexStringBuffer = new StringBuilder();
        for (byte b : byteArray) {
            hexStringBuffer.append(byteToHex(b));
        }
        return hexStringBuffer.toString();
    }

}
