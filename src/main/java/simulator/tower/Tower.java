package simulator.tower;

import simulator.flyable.Flyable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class Tower {
    private final List<Flyable> observers = new ArrayList<>();
    private List<Flyable> landed;

    public void register(Flyable flyable) throws IOException {
        observers.add(flyable);
        Launcher.writer.write("Tower Says: "
                + flyable.getType() + "#" + flyable.getName() + "(" + flyable.getId() + ")"
                + " registered to weather tower.\n");
    }

    public void unregister(Flyable flyable) {
        observers.remove(flyable);
    }

    public void landNewFlyable(Flyable flyable) throws IOException {
        landed.add(flyable);
        Launcher.writer.write("Tower Says: "
                + flyable.getType() + "#" + flyable.getName() + "(" + flyable.getId() + ")"
                + " unregistered from weather tower.\n");
    }

    protected void conditionsChanged() throws IOException {
        landed = new ArrayList<>();
        for (Flyable observer : observers) {
            observer.updateConditions(); // +всех приземелнных заносит в лист landed
        }
        for (Flyable observer : landed) {
            unregister(observer); //удаляет всех севших после хода
        }
    }
}
