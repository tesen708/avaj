package simulator.tower;

import simulator.flyable.Coordinates;
import java.util.Random;


public class WeatherProvider {

    static private WeatherProvider weatherProvider;
    static private final String[] weather = new String[]{"SUN", "RAIN", "FOG", "SNOW"};

    static private final Random random = new Random();

    private WeatherProvider() {
    }

    public static WeatherProvider getProvider() {
        if (weatherProvider == null) {
            weatherProvider = new WeatherProvider();
        }
        return weatherProvider;
    }

    //Возможно это метод другого класса. Иначе зачем существует changeWeather() в WeatherTower.
    // И вообеще генератор не написан. Просто заглушку возвращает.
    // UPD: К какому методу этот комментарий? Все еще актуально?

    public String getCurrentWeather(Coordinates coordinates) {
        getProvider();
        int index = (coordinates.getLatitude() + coordinates.getHeight() + coordinates.getLongitude() + Math.abs(random.nextInt())) % weather.length;
        return weather[index];
    }

    public static int getWeatherIndex(String weatherName) throws IllegalArgumentException {
        for (int i = 0; i < weather.length; i++) {
            if (weather[i].equals(weatherName)) {
                return i;
            }
        }
        throw new IllegalArgumentException("Не удалось понять погоду: " + weatherName);
    }

}

