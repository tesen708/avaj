package simulator.tower;

import simulator.flyable.Flyable;
import simulator.flyable.exceptions.InvalidContentException;
import simulator.scenario.ScenarioReader;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

public class Launcher {

    String OUTPUT_FILENAME = "simulation.txt";
    boolean LOG_MODE = false;
    static public BufferedWriter writer;

    public void start(String[] args) {
        List<Flyable> flyableList;

        writer = null;

        if (args.length == 1) {
            ScenarioReader readedData = read_input_file(args[0]);
            if (readedData == null) {
                return;
            }
            flyableList = readedData.getFlyableList();

            WeatherTower tower = new WeatherTower();

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_FILENAME, LOG_MODE)))
            {
                if (LOG_MODE) {
                    writer.write("\n----------------------- NEW SIMULATION -----------------------\n");
                }
                Launcher.writer = writer;
                registerAll(flyableList, tower);
                doIterations(tower, readedData.getSimulationRepetition());
            } catch(IOException e) {
                System.out.println("Испытываем проблему с выходным файлом \"" + OUTPUT_FILENAME + "\": " + e.getMessage());
            }

        } else if (args.length == 0) {
            System.out.println("Не пойму чего вы от меня хотите... Мне нужен файл сценария. (Например, 'scenario.txt')");
        } else {
            System.out.println("Не пойму чего вы от меня хотите... Слишком много параметров, я запутался. (Нужен один параметр)");
        }

    }

    public ScenarioReader read_input_file(String filename) {
        ScenarioReader scenarioReader;

        try {
            scenarioReader = ScenarioReader.createScenario(filename);
            return scenarioReader;
        } catch (FileNotFoundException e) {
            System.out.println("Не удалось прочитать файл: " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Произошла ошибка при чтении сценария: Неверный формат числа" + e.getMessage().substring(e.getMessage().indexOf(':')));
        } catch (InvalidContentException e) {
            System.out.println("Произошла ошибка при чтении сценария: " + e.getMessage());
        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            System.out.println("Произошла ошибка при создании списка пилотов: " + Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            System.out.println("Ой-ой... Произошла непредвиденная ошибка: " + e.getMessage());
        }
        return null;
    }

    private void registerAll(List<Flyable> flyableList, WeatherTower tower) throws IOException {
        for (Flyable flyable : flyableList) {
            tower.register(flyable);
            flyable.registerTower(tower);
        }
    }

    private void doIterations(WeatherTower tower, int iterations) throws IOException {
        for (int i = 0; i < iterations; i++) {
            tower.changeWeather();
        }
    }

}
