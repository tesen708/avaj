package simulator.flyable;

import java.lang.reflect.InvocationTargetException;

public abstract class AircraftFactory {

    static public Flyable newAircraft(String type, String name, int longitude, int latitude, int height) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Coordinates coordinates = new Coordinates(longitude, latitude, height);

        return (Flyable)Class.forName("simulator.flyable." + type)
                .getDeclaredConstructor(String.class, Coordinates.class)
                .newInstance(name, coordinates);
    }
}
