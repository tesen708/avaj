package simulator.flyable.exceptions;

public class InvalidContentException extends Exception {

    public InvalidContentException(String message) {
        super(message);
    }

}
